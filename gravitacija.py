
def grav(visina):
    C = 6.674 * (10**-11)
    M = 5.972 * (10**24)
    r = 6.371 * (10**6)
    a = C * M / ((r + visina)**2)
    return a
    
a = int(input())
print("Visina: {0} m\ng: {1} m/s^2".format(a,grav(a)))